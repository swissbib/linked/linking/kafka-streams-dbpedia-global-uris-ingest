/*
 * kafka-streams-dbpedia-global-uris-ingest
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.http.HttpHost
import org.apache.logging.log4j.Logger
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.GetIndexRequest
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import java.net.ConnectException
import java.util.*
import kotlin.system.exitProcess

class ElasticIndex(private val index: String, private val properties: Properties,
                   private val log: Logger
) {
    private val elastic = connect()

    private fun connect(): RestHighLevelClient {
        return RestHighLevelClient(
            RestClient.builder(
                HttpHost(properties.getProperty("elastic.host"), properties.getProperty("elastic.port").toInt())
            )
        )
    }

    init {
        try {
            when {
                elastic.indices().exists(GetIndexRequest(index), RequestOptions.DEFAULT) ->
                    log.info("The requested index {} exists. Good to go!", index)
                elastic.indices().existsAlias(GetAliasesRequest(index), RequestOptions.DEFAULT) ->
                    log.info("The requested alias {} exists. Good to go!", index)
                else -> {
                    log.error("Target index does not exist {}. Shutdown service ...", index)
                    exitProcess(1)
                }
            }
        } catch (ex: ConnectException) {
            log.error(
                "Could not connect to elasticsearch at {}:{} because of {}.",
                properties.getProperty("elastic.host"),
                properties.getProperty("elastic.port"), ex.message
            )
            exitProcess(1)
        }
    }

    fun get(field: String, searchTerm: String): SearchResponse {
        return elastic.search(createRequest(field, searchTerm, emptyArray(), emptyArray()), RequestOptions.DEFAULT)
    }

    fun getLabel(field: String, searchTerm: String, include: Array<String>, exclude: Array<String>): SearchResponse {
        return elastic.search(createRequest(field, searchTerm, include, exclude), RequestOptions.DEFAULT)
    }

    private fun createRequest(field: String, value: String, include: Array<String>, exclude: Array<String>): SearchRequest {
        return SearchRequest(index)
            .source(
                SearchSourceBuilder.searchSource().fetchSource(include, exclude)
                .query(QueryBuilders.termQuery(field, value)))
    }
}