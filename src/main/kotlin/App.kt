/*
 * kafka-streams-dbpedia-global-uris-ingest
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.LogManager
import java.util.*
import java.util.concurrent.CountDownLatch
import kotlin.system.exitProcess

class App {
    companion object {
        private val log = LogManager.getLogger("DBpediaGlobalUrisMerger")
        @JvmStatic fun main(args: Array<String>) {
            val version = Properties()
            version.load(ClassLoader.getSystemResourceAsStream("version.properties"))
            log.info("Application Version: ${version.getProperty("version")}")
            val props = KafkaProperties(log)
            run(
                KafkaStreams(
                    KafkaTopology(props.appProperties, log).build(), props.kafkaProperties))
        }

        private fun run(streams: KafkaStreams) {
            val latch = CountDownLatch(1)
            // attach shutdown handler to catch control-c
            Runtime.getRuntime().addShutdownHook(object : Thread("streams-shutdown-hook") {
                override fun run() {
                    streams.close()
                    latch.countDown()
                }
            })

            try {
                streams.start()
                latch.await()
            } catch (e: Throwable) {
                exitProcess(1)
            }

            exitProcess(0)
        }
    }
}