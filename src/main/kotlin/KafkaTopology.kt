/*
 * kafka-streams-dbpedia-global-uris-ingest
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.util.*
import java.util.regex.Pattern

class KafkaTopology(private val properties: Properties, private val log: Logger) {

    private val index = properties.getProperty("elastic.search.index")
    private val jsonParser = JsonParser(log)
    private val elasticIndex = ElasticIndex(index, properties, log)

    private val validWikidataPattern = Pattern.compile("http://www\\.wikidata\\.org/entity/Q\\d+")
    private val validDbpediaPatterns = Pattern.compile("http://((de|it|fr|commons).)?dbpedia\\.org/resource/.+")
    private val otherDbpediaPatterns = Pattern.compile("http://([a-z]{2,3}|simple).dbpedia\\.org/resource/.+")
    private val validGlobalDbpediaPattern = Pattern.compile("https://global\\.dbpedia\\.org/id/[0-9a-zA-Z]+")
    private val validViafPattern = Pattern.compile("http://(www.)?viaf\\.org/viaf/[\\dX]+")
    private val validGndPattern = Pattern.compile("http://d-nb.info/gnd/[\\d-Xx]+")

    fun build(): Topology {
        val builder = StreamsBuilder()

        val branchedStream = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .mapValues { value -> Pair(value.esIndexName, value.data) }
            .flatMapValues { value -> parseMessage(value) }
            .flatMapValues { readOnlyKey, value -> fetchGlobalUris(readOnlyKey, value) }
            .branch(
                Predicate { _, value -> value.second.inputStructure.owlSameAs.isEmpty() },
                Predicate { _, value -> value.second.inputStructure.owlSameAs.isNotEmpty() }
            )

        appendTail(branchedStream[0]
            .mapValues { value -> noInput(value) })


        appendTail(branchedStream[1]
            .mapValues { value -> mergeInput(value) })

        return builder.build()
    }

    private fun appendTail(stream: KStream<String, Pair<String, List<String>>>) {
        stream
            .mapValues { value -> filterUnusedUris(value) }
            .mapValues { value -> packResult(value) }
            .mapValues { value -> createSbMetadataModel(value) }
            .to(properties.getProperty("kafka.topic.sink"))
    }

    private fun parseMessage(value: Pair<String, String>): List<Pair<String, TypedInputStructure>> {
        return if (value.second == "{}") {
            listOf(Pair(value.first, TypedInputStructure(emptyList())))
        } else {
            when (val result = jsonParser.parseObject<InputStructure>(value.second)) {
                is InputStructure -> listOf(Pair(value.first, result.toTypedInputStructure()))
                else -> emptyList()
            }
        }
    }
    private fun fetchGlobalUris(key: String, value: Pair<String, TypedInputStructure>): List<Pair<String, InputAndSearchResponse>> {
        val response = elasticIndex.get("dbpedia", key)
        return when  {
            response.hits.hits.isEmpty() -> {
                log.error("Could not find key $key in global uris index.")
                emptyList()
            }
            response.hits.hits.size == 1 -> {
                val parsedResponse = jsonParser.parseObject<SearchResponse>(response.hits.hits[0].sourceAsString)

                return if (parsedResponse != null) {
                    val withGlobalId = SearchResponse(parsedResponse.dbpedia + response.hits.hits[0].id)
                    log.info("Enriched uri $key with global uris!")
                    listOf(Pair(value.first, InputAndSearchResponse(value.second, withGlobalId)))
                } else {
                    emptyList()
                }
            }
            else -> {
                log.error("Found more than one global uris for key $key.")
                emptyList()
            }
        }

    }

    private fun mergeInput(value: Pair<String, InputAndSearchResponse>): Pair<String, List<String>> {
        return Pair(value.first, (value.second.inputStructure.owlSameAs.toSet() + value.second.searchResponse.dbpedia.toSet()).toList())
    }

    private fun noInput(value: Pair<String, InputAndSearchResponse>): Pair<String, List<String>>  {
        return Pair(value.first, value.second.searchResponse.dbpedia)
    }

    private fun filterUnusedUris(value: Pair<String, List<String>>): Pair<String, List<String>> {
        val result = mutableListOf<String>()
        for (uri in value.second) {
            when {
                validDbpediaPatterns.matcher(uri).matches() -> result.add(uri)
                otherDbpediaPatterns.matcher(uri).matches() -> log.debug("Unsued dbpedia uri: $uri")
                validGlobalDbpediaPattern.matcher(uri).matches() -> result.add(uri)
                validWikidataPattern.matcher(uri).matches() -> result.add(uri)
                validViafPattern.matcher(uri).matches() -> result.add(uri)
                validGndPattern.matcher(uri).matches() -> result.add(uri)
                else -> log.error("Unknown URI encountered $uri.")
            }

        }
        return Pair(value.first, result)
    }

    private fun packResult(value: Pair<String, List<String>>): Pair<String, ElasticDocUpdate> {
        return Pair(value.first, ElasticDocUpdate(value.second))
    }

    private fun createSbMetadataModel(value: Pair<String, ElasticDocUpdate>): SbMetadataModel {
        return SbMetadataModel()
            .setData(jsonParser.write(value.second))
            .setEsIndexName(value.first)
            .setEsBulkAction(EsBulkActions.UPDATE)
    }
}
