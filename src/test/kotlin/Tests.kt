/*
 * kafka-streams-dbpedia-global-uris-ingest
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.kafka.streams.test.OutputVerifier
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.types.EsBulkActions
import org.swissbib.types.EsMergeActions
import java.io.File
import java.nio.charset.Charset


class Tests {
    private val log = LogManager.getLogger()
    private val props = KafkaProperties(log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties);

    private val resourcePath = "src/test/resources"
    private fun readFile(fileName: String): String {
        return File(resourcePath + fileName).readText(Charset.defaultCharset())
    }

    @Test
    fun testCase1() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"), "http://dbpedia.org/resource/1964_Coppa_Italia_Final", SbMetadataModel().setData("{}").setEsIndexName("es-index").setEsMergeAction(EsMergeActions.NEW)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("http://dbpedia.org/resource/1964_Coppa_Italia_Final", output.key())
        assertEquals("es-index", output.value().esIndexName)
        assertEquals(EsBulkActions.UPDATE, output.value().esBulkAction)
        assertEquals("{\"doc\" : {\"owl:sameAs\": [\"http://www.wikidata.org/entity/Q56367796\", \"http://dbpedia.org/resource/1964_Coppa_Italia_Final\", \"https://global.dbpedia.org/id/8JUtw\"]}}", output.value().data)

    }

    @Test
    fun testCase2() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(
            props.appProperties.getProperty("kafka.topic.source"),
            "http://dbpedia.org/resource/Andreas_Bjørn",
            SbMetadataModel().setData("{\"owl:sameAs\":\"http://commons.dbpedia.org/resource/Andreas_Bjørn\"}")
                .setEsIndexName("es-index")
                .setEsMergeAction(EsMergeActions.NEW)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("http://dbpedia.org/resource/Andreas_Bjørn", output.key())
        assertEquals("es-index", output.value().esIndexName)
        assertEquals(EsBulkActions.UPDATE, output.value().esBulkAction)
        assertEquals("{\"doc\" : {\"owl:sameAs\": [\"http://commons.dbpedia.org/resource/Andreas_Bjørn\", \"http://dbpedia.org/resource/Andreas_Bjørn\", \"http://www.wikidata.org/entity/Q12301515\", \"https://global.dbpedia.org/id/GyjX\"]}}", output.value().data)

    }
}