## Dbpedia Global URI Enricher

This mirco service uses the id-managment dataset of the dbpedia release and 
enriches the sameAs relations to each resource. In addition the global URI
is enriched as well.

Documentation: [http://dev.dbpedia.org/ID_and_Clustering](http://dev.dbpedia.org/ID_and_Clustering)


